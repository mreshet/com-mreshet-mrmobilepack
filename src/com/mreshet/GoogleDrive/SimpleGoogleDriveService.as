package com.mreshet.GoogleDrive
{
	import com.mreshet.mrFacebook.co.uk.mikestead.net.URLFileVariable;
	import com.mreshet.mrFacebook.co.uk.mikestead.net.URLRequestBuilder;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	public class SimpleGoogleDriveService
	{
		private var _ufv:							URLFileVariable 	= null;
		private var _urb:							URLRequestBuilder = null;
		private var _urv:							URLVariables 			= null;
		private var _urUpload:				URLRequest 				= null;
		private var _ulUpload:				URLLoader 				= null;
		private var _urSearch:				URLRequest 				= null;
		private var _ulSearch:				URLLoader 				= null;
		private var _urDownload:			URLRequest 				= null;
		private var _ulDownload:			URLLoader 				= null;
		private var _accessToken:			String 						= null;
		
		public var onActionComplete:	Function 					= null;
		public var onFailed:					Function 					= null;
		
		public function SimpleGoogleDriveService()
		{
			_urb				=	new URLRequestBuilder();
			_ufv				=	new URLFileVariable();
			_urv				=	new URLVariables();
			_urUpload   = new URLRequest();
			_ulUpload 	= new URLLoader();
			
			_urSearch   = new URLRequest();
			_ulSearch 	= new URLLoader();
			
			_urDownload = new URLRequest();
			_ulDownload = new URLLoader();
			
			_ulUpload.addEventListener(Event.COMPLETE, ulUpload_onComplete);
			_ulUpload.addEventListener(IOErrorEvent.IO_ERROR, ulUpload_onError);
			_ulUpload.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);

			_ulSearch.addEventListener(Event.COMPLETE, ulSearch_onComplete);
			_ulSearch.addEventListener(IOErrorEvent.IO_ERROR, ulSearch_onError);
			_ulSearch.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
			
			_ulDownload.addEventListener(Event.COMPLETE, ulDownload_onComplete);
			_ulDownload.addEventListener(IOErrorEvent.IO_ERROR, ulDownload_onError);
			_ulDownload.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
		}
		
		/**
		 * 
		 * @param file ByteArray or File
		 * 
		 */
		public function upload(file:Object, fileName:String = "sucare_backup"):void
		{
			_urUpload.url 				= "https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart";
			_urUpload.method 			= URLRequestMethod.POST;
			_urUpload.contentType = "multipart/related";

			var ba:	ByteArray 		= null;
			
			if(file is File) {
				var f:	File 				= file as File;
				
				var fs:	FileStream 	= new FileStream();
				ba 									= new ByteArray();
				fs.open(f, FileMode.READ);
				fs.readBytes(ba, 0, fs.bytesAvailable);
				fs.close();
			}
			else if( file is ByteArray) {
				ba = file as ByteArray;
			}
			
			ba.position 					= 0;
						
			_urv["json"] 					= '{"title": "' + fileName + '"}';
			_urv["file"]					= new URLFileVariable(ba, "ba");
			
			_urb 									= new URLRequestBuilder(_urv);
			_urUpload 						= _urb.build(_urUpload);
			
			var aa:	String 				= (_urUpload.data as ByteArray).toString();
						
			_ulUpload.load(_urUpload);
		}

		protected function ulUpload_onError(event:IOErrorEvent):void
		{
			trace("error: " + event);
//			if(onActionComplete)
				//onActionComplete(null);

//			return;
			
			if(onFailed)
				onFailed();
		}
		
		protected function ulUpload_onComplete(event:Event):void
		{
			trace(event.currentTarget.data);
			
			var res:Object = JSON.parse(String(event.currentTarget.data));
			
			if(onActionComplete)
				onActionComplete(res);
		}

		private function search(name:String):void
		{
			_urSearch.url 		= "https://www.googleapis.com/drive/v2/files?q=title='" + name + "'";
			_urSearch.method 	= URLRequestMethod.GET;
			
			_ulSearch.load(_urSearch);
		}
		
		public function download(name:String):void
		{
			search(name);
		}
		
		protected function ulSearch_onError(event:IOErrorEvent):void
		{
			trace("error: " + event);
			
			if(onFailed)
				onFailed();
		}
				
		protected function ulSearch_onComplete(event:Event):void
		{
			var res:Object 					= JSON.parse(String(event.currentTarget.data));

			_urDownload.url 				= res.items[0].downloadUrl;
			_urDownload.method 			= URLRequestMethod.GET;
			_ulDownload.dataFormat 	= URLLoaderDataFormat.BINARY;
			
			_ulDownload.load(_urDownload);
		}
		
		protected function ulDownload_onError(event:IOErrorEvent):void
		{
			trace("error: " + event);
			
			if(onFailed)
				onFailed();
			
		}
		
		protected function ulDownload_onComplete(event:Event):void
		{
			var data:	ByteArray = event.currentTarget.data as ByteArray;
			
			if(onActionComplete)
				onActionComplete(data);
			trace("ulDownload_onComplete");
		}
		
		protected function onHttpStatus(event:HTTPStatusEvent):void
		{
			trace("http status: " + event.status);
		}

		public function get accessToken():String	{	return _accessToken;	}
		public function set accessToken(value:String):void
		{
			_accessToken = value;
			
			_urUpload.requestHeaders.push(new URLRequestHeader("Authorization", "OAuth " + _accessToken));
			_urSearch.requestHeaders.push(new URLRequestHeader("Authorization", "OAuth " + _accessToken));
			_urDownload.requestHeaders.push(new URLRequestHeader("Authorization", "OAuth " + _accessToken));
		}

	}
}