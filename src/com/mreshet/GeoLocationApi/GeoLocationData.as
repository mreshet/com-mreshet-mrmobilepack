package com.mreshet.GeoLocationApi
{
	public class GeoLocationData
	{
		private var _lat:		Number;
		private var _long:	Number;
		private var _accu:	Number;
		private var _speed:	Number;
		
		public function GeoLocationData()
		{
		}

		public function get lat():							Number	{	return _lat;	}
		public function set lat(value:Number):	void
		{
			_lat = value;
		}

		public function get long():							Number	{	return _long;	}
		public function set long(value:Number):	void
		{
			_long = value;
		}

		public function get accu():							Number	{	return _accu;	}
		public function set accu(value:Number):	void
		{
			_accu = value;
		}

		public function get speed():						Number	{	return _speed;	}
		public function set speed(value:Number):void
		{
			_speed = value;
		}


	}
}