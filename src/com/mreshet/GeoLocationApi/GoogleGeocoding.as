package com.mreshet.GeoLocationApi
{
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class GoogleGeocoding
	{
		//https://maps.googleapis.com/maps/api/geocode/json?latlng=32.82306045759469,34.9914344586432&result_type=administrative_area_level_2|locality&key=AIzaSyCQyU1aYkqkZ9aEX-B1znU-ZMt7Y-5aMCQ
		static public const PATH_GOOGLE_GEOCODING_API:	String 				= "https://maps.googleapis.com/maps/api/geocode/json";
		
		private var _ul:																URLLoader 		= null;
		private var _ur:																URLRequest 		= null;
		private var _uv:																URLVariables 	= null;
		
		private var _keyGoogleGeocoding:								String				=	null;
		
		private var _lastGeocodingData:									Object				=	null;
		
		public var onUpdate:														Function			=	null;
		
		public function GoogleGeocoding($keyGoogleGeocoding:String)
		{
			_keyGoogleGeocoding	=	$keyGoogleGeocoding;
			
			_uv 								= new URLVariables();
			_ur 								= new URLRequest(PATH_GOOGLE_GEOCODING_API);
			_ul 								= new URLLoader();
			
			_uv["key"] 					= _keyGoogleGeocoding;
			_uv["language"] 		= "he";
			_uv["result_type"] 	= "route";//"administrative_area_level_2|locality";
			
			_ur.method 					= URLRequestMethod.GET;
			_ur.data 						= _uv;
			
			_ul.addEventListener(IOErrorEvent.IO_ERROR, onRequestError);
			_ul.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);

		}

		public function requestGeocoding(lat:Number, long:Number, $onUpdate:Function = null, $useCachedDataIfAvailable:Boolean = true):void
		{
			onUpdate 			= $onUpdate;
			
			if($useCachedDataIfAvailable && _lastGeocodingData) {
				if(onUpdate is Function)
					onUpdate();
			}
				
			_uv["latlng"] = lat + "," + long;
			
			_ul.addEventListener( Event.COMPLETE, onRequestResponse);

			_ul.load(_ur);
		}
		
		protected function onRequestResponse(event:Event):void
		{
			var res:	Object = JSON.parse(event.currentTarget.data);
			trace("onRequestResponse : " + event.currentTarget.data);
			
			_lastGeocodingData	=	res;
			
			if(onUpdate is Function)
				onUpdate();
		}
		
		public function formattedData():String
		{
			var res:			Object = _lastGeocodingData;
			
			if(res.status != "OK")
				return null;
			
			//var locality:	String = res.results[0].address_components[0].short_name;
			var street:			String = res.results[0].address_components[0].short_name;
			var city:			String = res.results[0].address_components[1].short_name;
			
			return street + ", " + city;
		}

		protected function httpStatusHandler(event:HTTPStatusEvent):void
		{
			trace("httpStatusHandler : " + event.status);
		}
		
		protected function onRequestError(event:IOErrorEvent):void
		{
			trace("onRequestError: " + event.errorID);
		}

		public function get lastGeocodingData():Object
		{
			return _lastGeocodingData;
		}

	}
}