package com.mreshet.GeoLocationApi
{
	import flash.events.GeolocationEvent;
	import flash.sensors.Geolocation;

	/**
	 * 
	 * @author Tomer
	 * 
	 */
	public class GeoLocationService
	{
		private var _geoService:							Geolocation						=	null;
		private var _lastGeoLocationData:			GeoLocationData				=	null;
		
		private var _googleGeocoding:					GoogleGeocoding				=	null;
		
		private var _flagAlwaysUpdate:				Boolean								=	false;
		
		public var geoCoding_onUpdate:				Function							=	null;
		
		public var DEBUG_MODE:								Boolean								=	false;

		public function GeoLocationService($googleGeocodingKey:String)
		{
			_googleGeocoding = new GoogleGeocoding($googleGeocodingKey);
		}
		
		public function requestUpdate():void
		{
			var geoHasEvent:	Boolean = _geoService.hasEventListener(GeolocationEvent.UPDATE);

			if(_flagAlwaysUpdate == true && geoHasEvent)
				return;
			
			_geoService.addEventListener(GeolocationEvent.UPDATE, geolocationUpdateHandler);
		}
		
		public function stop():void
		{
			_geoService.removeEventListener(GeolocationEvent.UPDATE, geolocationUpdateHandler);
		}
		
		public function initGeoLocation():void
		{
			
			//_googleGeocoding.requestGeocoding(32.842681, 35.079493);
			//return;

			if(_geoService)
				return;
			
			trace("Geolocation.isSupported " + Geolocation.isSupported);
			if (Geolocation.isSupported)
			{
				//Initialize the location sensor.
				_geoService = new Geolocation();
				trace("Geolocation.isMuted " + _geoService.muted);

				if(! _geoService.muted){
					_geoService.setRequestedUpdateInterval(5000);
					//Register to receive location updates.
					_geoService.addEventListener(GeolocationEvent.UPDATE, geolocationUpdateHandler);
				}
			}
			else {
			}
		}
		
		protected function geolocationUpdateHandler(event:GeolocationEvent):void
		{
			if(_flagAlwaysUpdate == false)
				_geoService.removeEventListener(GeolocationEvent.UPDATE, geolocationUpdateHandler);
			
			trace("new Geo Location info")
			_lastGeoLocationData				=	new GeoLocationData();
			_lastGeoLocationData.lat 		=  event.latitude;
			_lastGeoLocationData.long 	=  event.longitude;
			_lastGeoLocationData.accu 	=  event.horizontalAccuracy;
			_lastGeoLocationData.speed 	=  event.speed;
			
			requestGeocoding();
		}

		public function requestGeocoding($geoCoding_onUpdate:Function = null, $useCachedDataIfAvailable:Boolean = true):void
		{
			trace("requestGeocoding " + _lastGeoLocationData ? "start" : "No locationData yet" );
			
			if($geoCoding_onUpdate)
				geoCoding_onUpdate	=	$geoCoding_onUpdate;
			
			if(DEBUG_MODE) {
				_googleGeocoding.requestGeocoding(32.842681, 35.079493, geoCoding_onUpdate, $useCachedDataIfAvailable);
				return;
			}
			
			if(_lastGeoLocationData)
				_googleGeocoding.requestGeocoding(_lastGeoLocationData.lat, _lastGeoLocationData.long, geoCoding_onUpdate, $useCachedDataIfAvailable);
		}

		public function lastGeocodingData(formatToString:Boolean = true):Object
		{
			if(formatToString)
				return _googleGeocoding.formattedData();
			return _googleGeocoding.lastGeocodingData;
		}
		
		public function get lastGeoLocationData():GeoLocationData
		{
			return _lastGeoLocationData;
		}
		
		public function get flagAlwaysUpdate():								Boolean	{	return _flagAlwaysUpdate;	}
		public function set flagAlwaysUpdate(value:Boolean):	void
		{
			_flagAlwaysUpdate = value;
		}

	}
}