package com.mreshet.common.interfaces
{
  import com.mreshet.mrProcessManager.core.interfaces.IId;
  
  public interface IIdDisposable extends IId, IDisposable
  {
  }
}