package com.mreshet.common.interfaces
{
  public interface IDisposable
  {
    /**
     * dispose the item
     */
    function dispose():void;
  }
}