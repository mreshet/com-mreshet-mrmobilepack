package com.mreshet.common.interfaces
{
	public interface IData
	{
		function get data(): 							Object;
		function set data(value: Object): void;
	}
}