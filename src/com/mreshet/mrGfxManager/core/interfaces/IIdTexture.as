package com.mreshet.mrGfxManager.core.interfaces
{	
	import com.mreshet.mrContentManager.core.interfaces.IId;
	
	import starling.textures.Texture;

	public interface IIdTexture extends IId
	{
		function get tex(): Texture;
		function set tex(value: Texture): void;
	}
}