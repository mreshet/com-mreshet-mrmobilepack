package com.mreshet.mrDownloadManager
{
	import com.mreshet.mrDownloadManager.core.MrDownloadManagerSettings;
	import com.mreshet.mrDownloadManager.core.MrDownloadSessionStatus;
	import com.mreshet.mrDownloadManager.core.MrDownloadStatus;
	
	import flash.utils.Dictionary;

	public class MrDownloadManager
	{
		
		private var _mrLocalDownloads:			Vector.<MrDownloadLocal>	=	null;
		
		private var _numDownloadsLeft:			uint											=	0;
		
		private var _cb_onSessionComplete:	Function									=	null;
		
		private var _downloadsByIdDic:			Dictionary								=	null;
		
		private var _totalFailed:						uint											=	0;
		
		private var _isSessionStarted:			Boolean										=	false;
		
		private var _totalSessionsTries:		uint											=	0;
		
		public function MrDownloadManager($cb_onSessionComplete:	Function	=	null)
		{
			_cb_onSessionComplete	=	$cb_onSessionComplete;
			_mrLocalDownloads			=	new Vector.<MrDownloadLocal>();
			_downloadsByIdDic			=	new Dictionary();
			_numDownloadsLeft			=	0;
		}
		
		/**
		 * returns true/false if download session can be started
		 */ 
		public function startDownloadSession():	Boolean
		{
			_totalFailed	=	0;
			
			if(_totalSessionsTries == MrDownloadManagerSettings.MAX_SESSION_DOWNLOAD_TRIES_PER_LOW_RETRY_FACTOR)
				return false; 
			
			if(_mrLocalDownloads.length == 0)
				return false;
			
			_numDownloadsLeft	=	_mrLocalDownloads.length;
			_isSessionStarted = true;
			
			var mrLocalDownload:MrDownloadLocal;
			var totalDownloads:uint	=	_mrLocalDownloads.length;
			for(var ix:uint	=	0; ix < totalDownloads;  ix++)	
			{
				mrLocalDownload	=	_mrLocalDownloads[ix];
				
				// if it is destnativepath is set then it means user wants to download to disk,
				// but we also check it that file is already there
				if(mrLocalDownload.fileExists && mrLocalDownload.destLocalNativePath)
					onDownloadComplete(new MrDownloadStatus(MrDownloadStatus.STATUS_WRITE_SUCCEED, mrLocalDownload.id));
				else
					mrLocalDownload.downloadFile();
			}
			
			_totalSessionsTries += 1;
			
			return true;
		}
		
		public function queueDownload($url:String, $downloadId: String	=	null, $writeToDisk:Boolean = true, $destLocalFolderPath:String = null):void
		{
			if($url == null)
				return;
			
			var ld:	MrDownloadLocal	=	new MrDownloadLocal($url, $destLocalFolderPath, onDownloadComplete, null, $downloadId, $writeToDisk);
			
			//if(ld.fileExists == false) {
			_mrLocalDownloads.push(ld);
			_downloadsByIdDic[ld.id] = ld;
			//}
			
			if(_isSessionStarted)
				ld.downloadFile();
		}
		
		public function getAllDownloads():Vector.<MrDownloadLocal>
		{
			return _mrLocalDownloads;
		}
		
		public function getDownloadByID($id:String):MrDownloadLocal
		{
			var dl:MrDownloadLocal	=	_downloadsByIdDic[$id] as MrDownloadLocal;
			return dl;
		}
		
		private function onDownloadComplete($status:MrDownloadStatus):void
		{
			/**
			 * check status
			 */ 
			if($status.status	== MrDownloadStatus.STATUS_FAILED) 
			{
				var dl:	MrDownloadLocal	=	getDownloadByID($status.id);
				if(dl.numTries <= MrDownloadManagerSettings.MAX_DOWNLOAD_TRIES_PER_FAILURE) 
				{
					dl.downloadFile();
					return;
				}
				
				_totalFailed += 1;
			}
			
			/**
			 */ 
			
			_numDownloadsLeft -= 1;
			
			if(_numDownloadsLeft == 0) {
				var dss:	MrDownloadSessionStatus = new MrDownloadSessionStatus(_mrLocalDownloads.length, _totalFailed);
				if(_cb_onSessionComplete is Function)
					_cb_onSessionComplete(dss);
			}
			
		}
		
		/**
		 * frees and disposes all the binary data,
		 * make sure you use it if you made a deep copy of the data
		 * other wise it will get lost 
		 * 
		 */
		public function free():void
		{
			trace("MrDownloadManager.free()")
			
			for (var k:Object in _downloadsByIdDic) {
				delete _downloadsByIdDic[k]
			}

			for(var ix:uint	=	0; ix < _mrLocalDownloads.length; ix++) {
				_mrLocalDownloads[ix].dispose();
				_mrLocalDownloads[ix] = null;
			}
						
			_mrLocalDownloads.length = 0;
			_mrLocalDownloads			=	null;
			_cb_onSessionComplete	=	null;
			_downloadsByIdDic			=	null;
		}

	}
}