package com.mreshet.mrDownloadManager.core
{
	public class MrDownloadStatus
	{
		public static var STATUS_DOWNLOAD_SUCCEED:	String		=	"STATUS_DOWNLOAD_SUCCEED";
		public static var STATUS_WRITE_SUCCEED:			String		=	"STATUS_WRITE_SUCCEED";
		public static var STATUS_FAILED:						String		=	"STATUS_FAILED";
		
		public var status:													String		=	null;
		public var id:															String		=	null;

		public function MrDownloadStatus($status:String, $id:String)
		{
			status	=	$status;
			id			=	$id;
		}
		
	}
}