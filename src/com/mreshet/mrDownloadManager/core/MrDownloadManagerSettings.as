package com.mreshet.mrDownloadManager.core
{
	public class MrDownloadManagerSettings
	{
		public static var MAX_DOWNLOAD_TRIES_PER_FAILURE:										uint	=	2;
		public static var MAX_SESSION_DOWNLOAD_TRIES_PER_LOW_RETRY_FACTOR:	uint	=	10;
		
	}
}