package com.mreshet.mrDownloadManager.core
{
	public class MrDownloadSessionStatus
	{
		public var totalFailed:			uint;
		public var totalDownloads:	uint;
		
		private var _retryFactor:		Number
		
		public function MrDownloadSessionStatus($totalDownloads:uint, $totalFailed:uint)
		{
			totalDownloads	=	$totalDownloads;
			totalFailed			=	$totalFailed;
			
			_retryFactor 		= (totalFailed / totalDownloads);	
		}
		
		public function get retryFactor():Number
		{
			return _retryFactor;
		}
	}
}