package com.mreshet.mrContentManager.core.interfaces
{
	public interface IId
	{
		function get id(): String;
		function set id(value: String): void;
	}
}