package com.mreshet.mrContentManager.examples
{
	import com.mreshet.mrContentManager.MrPackage;
	import com.mreshet.mrContentManager.MrContentManager;
	
	public class AppContent extends MrContentManager
	{
		
		public function AppContent()
		{
			super();
		}
		
		/**
		 * here deal with content processing like parsing etc for initial loading..
		 */
		public function loadInitialContent():void
		{
			var newBatch: MrPackage = addOrGetContentPackage("initBatch1");
			newBatch.enqueue("assets/data.xml", "appConfig1");
			newBatch.enqueue("assets/comps/splash/background.png", "splash1");
			newBatch.enqueue("assets/fonts/Nehama.ttf", "font1");
			newBatch.process(onInitBatchFinished);
		}

		private function onInitBatchFinished():void
		{
			//dispatchEvent(new Event(CONTENT_LOADED));
		}

		/*
		
		// for exmaple in external file
		
		_appContent = new AppContent();
		_appContent.addEventListener(ContentEvent.CONTENT_LOADED, onInitialContentLoaded);
		_appContent.loadInitialContent();
*/
		
		/*
		private function loadSettings():void
		{
			var xmlString:	String	=	null;
			var settXML:		XML			=	null;
			var file:				File 		= File.documentsDirectory.resolvePath("GynOnc/settings.xml");
			
			if(!file.exists)
				return;
			
			var fs:					FileStream	=	new FileStream();

			try{
				fs.open(file, FileMode.READ);
				xmlString =	fs.readUTFBytes(fs.bytesAvailable);
				settXML		=	new XML(xmlString);
				
			}
			catch(err:Error){
				trace(err);
			}
			finally {
				fs.close();
			}

		}
		*/
		
	}
}