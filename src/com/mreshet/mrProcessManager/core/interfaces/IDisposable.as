package com.mreshet.mrProcessManager.core.interfaces
{
  public interface IDisposable
  {
    /**
     * dispose the item
     */
    function dispose():void;
  }
}