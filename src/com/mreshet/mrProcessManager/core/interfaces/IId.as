package com.mreshet.mrProcessManager.core.interfaces
{
	public interface IId
	{
		/**
		 * id
		 */
		function get id(): String;
		function set id(value: String): void;
	}
}