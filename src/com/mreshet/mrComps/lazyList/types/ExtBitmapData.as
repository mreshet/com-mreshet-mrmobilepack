package com.mreshet.mrComps.lazyList.types
{
	import flash.display.BitmapData;
	
	import com.mreshet.ds.cache.core.interfaces.IIdDisposable;
	
	public class ExtBitmapData extends BitmapData implements IIdDisposable
	{
		private var _id: String = null;
		
		public function ExtBitmapData(width:int, height:int, transparent:Boolean=true, fillColor:uint=4.294967295E9)
		{
			super(width, height, transparent, fillColor);
		}
		
		public function get id():							String	{	return _id;		}
		public function set id(value:String):	void		{	_id = value;	}
	}
}