package com.mreshet.mrComps.mainWindow
{
	import com.mreshet.mrComps.flexComps.flexComp.FlexComp;
	import com.mreshet.mrComps.mainWindow.interfaces.IMainWindow;
	import com.mreshet.mrComps.mrScreenNavigator.MrScreenNavigator;
	
	import feathers.core.FeathersControl;
	import feathers.events.FeathersEventType;
	
	import starling.events.Event;
		
	/**
	 * base class for a Feather appication main Window container with navigator.
	 * extends this for more layouts. for example with action bar and a tab bar.
	 * @author Tomer Shalev
	 */
	public class MainWindow extends FlexComp implements IMainWindow
	{
		protected var _navigator:							MrScreenNavigator  												= null;

		public function MainWindow()
		{
		}
		
		override protected function initialize():void
		{
			_navigator	=	new MrScreenNavigator();

			_navigator.addEventListener(FeathersEventType.TRANSITION_COMPLETE, navigator_onTransitionComplete);
			_navigator.addEventListener(FeathersEventType.TRANSITION_START, navigator_onTransitionStart);
			
			addChildAt(_navigator, 0);
			
			_navigator.removeAllScreens(); 
		}
		
		/**
		 * by default navigator takes up all screen
		 */
		override protected function draw():void
		{
			super.draw();
			
			_navigator.width 	= width;
			_navigator.height = height;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_navigator = null;
		}

		/**
		 * override this to listen to transition events 
		 * @param event
		 */
		public function navigator_onTransitionComplete(event: Event): void
		{
			trace(_navigator.activeScreen);
		}
		
		/**
		 * override this to listen to transition events 
		 * @param event
		 */
		public function navigator_onTransitionStart(event: Event): void
		{
			trace(_navigator.activeScreen);
		}
		
		/**
		 * @inheritDoc
		 */
		public function get navigator():MrScreenNavigator
		{
			return _navigator;
		}
	}
}