package com.mreshet.mrComps.mainWindow.ext.hamMainWindow
{
	public class HamProperties
	{
		public var animateOpenPopUpTime:	Number	=	0.5;
		public var animateClosePopUpTime:	Number	=	0.5;
		public var animateToXPercent:			Number	=	0.0;
		public var animateFromXPercent:		Number	=	0.0;
		
	}
}