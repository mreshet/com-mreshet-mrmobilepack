package com.mreshet.mrComps.mainWindow.interfaces
{
	import com.mreshet.mrComps.mrScreenNavigator.MrScreenNavigator;
	
	import starling.events.Event;

	public interface IMainWindow
	{
		/**
		 * get the navigator 
		 * @return MrScreenNavigator
		 * 
		 */
		function get navigator():MrScreenNavigator;
		
		/**
		 * event listener for when a transition is complete 
		 * @param event
		 * 
		 */
		function navigator_onTransitionComplete(event: Event): void;
		
		/**
		 * event listener for when a transition is starting 
		 * @param event
		 * 
		 */
		function navigator_onTransitionStart(event: Event): void;
	}
}