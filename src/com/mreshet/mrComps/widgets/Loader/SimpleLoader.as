package com.mreshet.mrComps.widgets.Loader
{
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	public class SimpleLoader extends Image
	{
		private var _tween:Tween = null;
		
		private var _heightOriginal:Number;
		private var _widthOriginal:Number;
		
		private var _resizedOnce:Boolean = false;
		
		public function SimpleLoader(texture:Texture)
		{
			super(texture);
			visible = false;

			super.pivotX = width / 2;
			super.pivotY = height / 2;
			
			_heightOriginal = height;
			_widthOriginal = width;
			
			_tween = new Tween(this, 2);
			_tween.repeatCount = 0;

			this.smoothing = TextureSmoothing.BILINEAR;
		}
		
		public function animateLoading():void
		{
			if(isAnimating)
				return;
			
			visible = true;
			_tween.reset(this, 0.5);
			_tween.repeatCount = 0;
			
			_tween.animate("rotation", rotation + Math.PI*2);
			
			Starling.juggler.add(_tween);
		}
		
		public function animateStop():void
		{
			visible = false;
			Starling.juggler.remove(_tween);
			rotation = 0;
		}
		
		public function get isAnimating():Boolean
		{
			return Starling.juggler.contains(_tween);
		}
		
		override public function set pivotX(value:Number):void
		{
		}
		
		override public function set pivotY(value:Number):void
		{
		}
		
		override public function set width(value:Number):void
		{
			//if(isAnimating)
			//	return;

			_widthOriginal = value;
			super.width = value;
		}
		
		override public function set height(value:Number):void
		{
			//if(isAnimating)
			//	return;

			_heightOriginal = value;
			super.height = value;
		}
		
		public function get heightOriginal():Number
		{
			return _heightOriginal;
		}

		public function get widthOriginal():Number
		{
			return _widthOriginal;
		}


	}
}