package com.mreshet.mrComps.animatedControl
{		
	import com.mreshet.mrComps.flexComps.flexComp.FlexComp;
	
	import feathers.core.FeathersControl;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.events.Event;
	
	public class AnimatedControl extends FlexComp
	{	
		public static var ANCHOR_TOP:						String					=	"ANCHOR_TOP";
		public static var ANCHOR_BOTTOM:				String					=	"ANCHOR_BOTTOM";
		
		protected var _animating:								Boolean;
		protected var _open:										Boolean;
		protected var _animateWhenAddedToStage:	Boolean;
		
		public var animateToY:									Number 					= NaN;
		public var animateFromY:								Number 					= NaN;
		public var animateToX:									Number 					= NaN;
		public var animateFromX:								Number 					= NaN;
		public var anchorPosition:							String					=	ANCHOR_TOP;
		
		public var animateOpenPopUpTime:				Number					=	0.7;
		public var animateClosePopUpTime:				Number					=	0.5;
		
		public var fitHeight:										Number;

		public var onClosed:										Function				=	null;
		public var onOpened:										Function				=	null;
		
		private var _tween:											Tween						=	null;
				
		public function AnimatedControl($animateWhenAddedToStage:Boolean)
		{
      super();
      
			_animateWhenAddedToStage	=	$animateWhenAddedToStage;

			_open											=	false;
			
			visible										=	true;
			
			_tween										=	new Tween(this, 0.5);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_tween = null;
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

			if(_animateWhenAddedToStage)
				animatePopUp();
		}

		public function toggle():void
		{
			if(_open)
				animateClosePopUp(true);
			else
				animatePopUp();
		}
		
		public function animatePopUp():void
		{
			visible											=	true;
			Starling.juggler.removeTweens(this);
		
			_tween.reset(this, animateOpenPopUpTime, Transitions.EASE_OUT);
			_animating									=	true;
			
			//validate();
			
			var finalAnimateToY:Number	=	isNaN(animateToY) ? y : animateToY;
			var finalAnimateToX:Number	=	isNaN(animateToX) ? x : animateToX;
			
			if(anchorPosition	== ANCHOR_BOTTOM) {
				finalAnimateToY	-= fitHeight;	
			}
				
			_tween.moveTo(finalAnimateToX, finalAnimateToY);
			_tween.delay								=	0.0;
			_tween.onComplete						=	onAniComplete;
			
			Starling.juggler.add(_tween);
		}
		
		public function animateClosePopUp($callBackAtComplete:Boolean	=	true):void
		{
			Starling.juggler.removeTweens(this);
			
			if(_tween == null)
				return;
			
			_tween.reset(this, animateClosePopUpTime, Transitions.LINEAR);
			_animating										=	true;
			
			var finalAnimateFromY:Number	=	isNaN(animateFromY) ? y : animateFromY;
			var finalAnimateFromX:Number	=	isNaN(animateFromX) ? x : animateFromX;

			_tween.moveTo(finalAnimateFromX, finalAnimateFromY);
			_tween.onComplete							=	onPopUpClosed;
			
			var args:	Array								=	new Array();
			args.push($callBackAtComplete);
			
			_tween.onCompleteArgs					=	args;
			
			Starling.juggler.add(_tween);
		}
		
		protected function onAniComplete():void
		{
			_animating 									= false;
			_open			 									=	true;
			
			if(onOpened is Function)
				onOpened();
		}
		
		protected function onPopUpClosed($callBackAtComplete:Boolean	=	true):void
		{
			//visible											=	false;
			
			Starling.juggler.removeTweens(this);
			
			_open												=	false;
			_animating 									= false;
			
			if($callBackAtComplete == false)
				return;
			
			if(onClosed is Function)
				onClosed();
		}
		
		public function get open():				Boolean	{	return _open;				}
		public function get animating():	Boolean	{	return _animating;	}

		public function set open(value:Boolean):void
		{
			_open = value;
		}


	}
}