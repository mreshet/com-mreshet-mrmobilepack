package com.mreshet.mrComps.roTable.core.roTableDataType
{
	public class RoTableDataType
	{
		private var _roTableName:					String											=	null;
		private var _roTableId:						String											=	null;
		private var _roRecords:						Vector.<RoRecordDataType>		=	null;
		
		private var _roColumnsProperties:	Vector.<RoColumnProperties>	=	null;
		
		public function RoTableDataType($roTableName:String, $roTableId:String)
		{
			_roTableName					=	$roTableName;
			_roTableId						=	$roTableId;
			
			_roRecords						=	new Vector.<RoRecordDataType>();
			_roColumnsProperties	=	new Vector.<RoColumnProperties>();
		}
		
		public function addColumnProperties($roColumnProperties:RoColumnProperties):void
		{
			_roColumnsProperties.push($roColumnProperties);
		}

		public function get roTableName():String
		{
			return _roTableName;
		}

		public function get roRecords():Vector.<RoRecordDataType>
		{
			return _roRecords;
		}

		public function get roTableId():String
		{
			return _roTableId;
		}

		public function get roColumnsProperties():Vector.<RoColumnProperties>
		{
			return _roColumnsProperties;
		}

	}
}