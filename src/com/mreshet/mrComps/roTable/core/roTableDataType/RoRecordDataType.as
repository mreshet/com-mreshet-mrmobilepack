package com.mreshet.mrComps.roTable.core.roTableDataType
{
	public class RoRecordDataType
	{
		private var _roFields:					Vector.<String>	=	null;

		public function RoRecordDataType($roFields:Vector.<String>)
		{
			_roFields			=	$roFields;
		}
				
		public function get roFields():Vector.<String>
		{
			return _roFields;
		}
	}
}