package com.mreshet.mrComps.textService.mrTLFSprite
{
	import flash.text.engine.FontLookup;
	
	import feathers.core.FeathersControl;
	
	import flashx.textLayout.formats.BlockProgression;
	import flashx.textLayout.formats.Direction;
	import flashx.textLayout.formats.TextLayoutFormat;
		
	public class MrTLFWrapper extends FeathersControl
	{
		private var _tlfSprite:TLFSprite	=	null;
		
		private var _text:String	= null;
		private var _textWidth:Number	= -1;
		private var _textHeight:Number	= -1;
		private var _fontSize:uint	=	0;
		private var _color:uint	=	0;
		private var _fontName:String	=	null;
		
			
		public function MrTLFWrapper($text:String, $width:Number, $height:Number, $fontSize:Number, $color:uint, $fontName:String	=	"Arial")
		{
			super();
			
			_textWidth	=	$width;
			_textHeight	=	$height;
			_text				= $text;
			_fontSize	=	$fontSize;
			_color		=	$color;
			_fontName = $fontName;
		}
		
		override public function dispose():void
		{
			super.dispose();
			if(_tlfSprite == null)
				return;
			removeChild(_tlfSprite);
			_tlfSprite.dispose();
			_tlfSprite	=	null;
		}
		
		override protected function initialize():void
		{
			super.initialize();
			
			//var ef:ElementFormat;
			//ef.
			
			_tlfSprite	=	setupRTL(_text, _textWidth, _textHeight);
			_tlfSprite.createRenderedBitmap();

			actualWidth	=	_textWidth;
			actualHeight = _tlfSprite.contentHeight;
			
			addChild(_tlfSprite);
		}
		
		private function setupRTL($str:String, $width:Number, $height:Number):TLFSprite
		{
			/*		
			var tlfSprite:TLFSprite = TLFSprite.fromPlainText("Hello World");
			tlfSprite.border = true;
			tlfSprite.x = 10;
			tlfSprite.y = 10;
			return tlfSprite;
			*/	
			var textLayoutFormat:TextLayoutFormat = new TextLayoutFormat();
			textLayoutFormat.fontLookup	= FontLookup.EMBEDDED_CFF;
			textLayoutFormat.fontFamily = _fontName;//"FbTipograf-Regular";
			textLayoutFormat.fontSize = _fontSize;
			textLayoutFormat.color	=	_color;
			textLayoutFormat.paragraphSpaceBefore = 2;
			textLayoutFormat.paragraphSpaceAfter = 2;
			textLayoutFormat.paddingBottom = textLayoutFormat.paddingTop = 
			textLayoutFormat.paddingLeft = textLayoutFormat.paddingRight = 5;
			textLayoutFormat.direction = Direction.RTL;
			textLayoutFormat.blockProgression = BlockProgression.TB;
			var tlfSprite:TLFSprite = TLFSprite.fromPlainText($str, textLayoutFormat);
			tlfSprite.border = false;
			tlfSprite.compositionWidth = $width;

			return tlfSprite;
		}

	}
}