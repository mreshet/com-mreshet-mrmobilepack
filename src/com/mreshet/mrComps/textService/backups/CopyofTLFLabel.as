package com.mreshet.mrComps.textService.backups
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.conversion.TextConverter;
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.factory.TextFlowTextLineFactory;
	import flashx.textLayout.factory.TruncationOptions;
	import flashx.textLayout.formats.BlockProgression;
	import flashx.textLayout.formats.Direction;
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.TextLayoutFormat;
	
	import starling.core.RenderSupport;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	/**
	 * a TLF label, supports RTL languages. promotes memory reusage and very optimal. borrows some
	 * source code from TLFSprite, but this is generally more performence (in place memory and 0 depth from it's container)
	 * <br><b>Notes:</b>
	 * <li>use <code>this.textLayoutFormat</code> to assign layout and fonts.</li>
	 * @author Tomer Shalev
	 */
	public class CopyofTLFLabel extends Image
	{
		private var _text:								String																=	null;
		private var _textLayoutFormat:		TextLayoutFormat 											= null;
		
		private var _bmText:							BitmapData	 													= null;
		
		private var _textFlow:						TextFlow 															= null;
		private var _TextLineFactory:			TextFlowTextLineFactory								=	null;
		private var _TruncationOptions:		TruncationOptions											=	null;
		private var _CompositionBounds:		Rectangle															=	null;
		private var _maxBounds:						Rectangle															=	null;
		private var _helperMatrix:				Matrix																=	null;
		
		private var _isInvalid:						Boolean																=	true;
		private var _autoResizeHeight:		Boolean																=	false;
		private var _autoResizeWidth:			Boolean																=	false;
		
		private var _actualContentBounds:	Rectangle															=	null;
		
		private var _backgroundColor:			uint																	=	0x00000000;
		
		private var _tex:Texture	=	null;
		
		private static var texsD:Dictionary = new Dictionary();
		private static var texsV:Vector.<Texture> = new Vector.<Texture>();
		
		
		/**
		 * a TLF label, supports RTL languages. promotes memory reusage and very optimal. borrows some
		 * source code from TLFSprite, but this is generally more performence (in place memory and 0 depth from it's container)
		 * <br><b>Notes:</b>
		 * <li>use <code>this.textLayoutFormat</code> to assign layout and fonts.</li>
		 */
		public function CopyofTLFLabel()
		{
			_TextLineFactory 		= new TextFlowTextLineFactory();
			_TruncationOptions 	= new TruncationOptions();
			_maxBounds				 	= new Rectangle( 0, 0, 512, 512);
			_CompositionBounds 	= new Rectangle( 0, 0, _maxBounds.width, _maxBounds.height);
			_helperMatrix 			= new Matrix();
			
			_bmText 						= new BitmapData(_CompositionBounds.width, _CompositionBounds.height);
			
			_TextLineFactory.truncationOptions 					= _TruncationOptions;
			
			_tex	=	Texture.fromBitmapData(_bmText, false);
			
			super(_tex);
			
			this.smoothing 			= TextureSmoothing.NONE;
			
			//			
		}
		
		public var _isWriting:Boolean = false;
		override public function render(support:RenderSupport, parentAlpha:Number):void
		{				
			if(_isInvalid) {
				
				_tex.root.uploadBitmapData(renderTextCore());
				//_tex.root.uploadBitmapData(_bmText);
				
				var h:	Number 	= (_autoResizeHeight ? _actualContentBounds.height : _CompositionBounds.height) / texture.height;
				var xx:	Number 	= (_autoResizeWidth ? _actualContentBounds.x : 0) 	/ texture.width;
				var w:	Number 	= _autoResizeWidth ? (xx + (_actualContentBounds.width) 	/ texture.width) : _CompositionBounds.width / texture.width;
				var yy:	Number 	= 0;
				
				this.setTexCoordsTo(0, 	xx, 	yy);
				this.setTexCoordsTo(1, 	w,		yy);
				this.setTexCoordsTo(2,	xx,		h);
				this.setTexCoordsTo(3,	w,		h);
				
				
			}
			
			super.render(support, parentAlpha);
			
			_isInvalid 			= false;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			if(_bmText) {
				_bmText.dispose();
				_bmText 									= null;
			}
			
			if(texture) {
				texture.root.dispose();
				texture.dispose();
			}			
			
			_helperMatrix								=	null;
			_CompositionBounds					=	null;
			_TruncationOptions					=	null;
			_TextLineFactory						=	null;
			
			_textFlow										=	null;
			_textLayoutFormat						=	null;
		}
		
		public function renderTextCore():	BitmapData
		{
			var tlfFormat:	TextLayoutFormat 						= _textLayoutFormat ? _textLayoutFormat : defaultTextLayoutFormat();
			var test:				String 											= "אני חושבת שמצאתי את התרופה לחיים מאושרים יותר... אני חושבת שמצאתי את התרופה שתהפוך אותך ליפה יותר למטופחת יותר ....אני חושבת שמצאתי את התרופה נגד ההתקרחות ... אני חושבת שמצאתי את התרופה לאלה שאיבדו את הצחוק את התרופה לאלה שאיבדו את הצורך לחיות את התרופה שתחזיר לכם את ההנאה את החיוך ... אתם רוצים לדעת מהי התרופה? גירושין !!! ";
			
			if(_text == null)
				_text 																		= test;
			
			_textFlow 																	= TextConverter.importToFlow(_text ? _text : "", TextConverter.PLAIN_TEXT_FORMAT);
			
			_TextLineFactory.compositionBounds 					= _CompositionBounds;
			
			_textFlow.hostFormat 												= tlfFormat;
			
			_bmText.fillRect(_bmText.rect, _backgroundColor);
			
			_TextLineFactory.createTextLines( generatedTextLineOrShape, _textFlow);
			
			_actualContentBounds				 								= _TextLineFactory.getContentBounds();
			
			if(_autoResizeHeight)
				super.height															=	 _actualContentBounds.height;
			
			if(_autoResizeWidth)
				super.width																=	 _actualContentBounds.width;
			
			return _bmText;
		}
		
		private function generatedTextLineOrShape(lineOrShape:flash.display.DisplayObject):	void	
		{	
			_helperMatrix.setTo(1, 0, 0, 1, lineOrShape.x, lineOrShape.y);
			_bmText.draw(lineOrShape, _helperMatrix);
			
		}
		
		private function defaultTextLayoutFormat():TextLayoutFormat
		{
			var tlFormat:	TextLayoutFormat	=	new TextLayoutFormat();
			
			tlFormat.color									=	0x3c3c3c;
			tlFormat.fontSize								=	17;
			tlFormat.blockProgression 			= BlockProgression.TB;
			tlFormat.fontFamily 						= "arial";
			tlFormat.textAlign 							= TextAlign.RIGHT;
			tlFormat.paragraphSpaceBefore 	= tlFormat.paragraphSpaceAfter = 2;
			tlFormat.paddingBottom 					= tlFormat.paddingTop = tlFormat.paddingLeft = tlFormat.paddingRight = 5;
			tlFormat.direction							=	Direction.RTL;
			
			return tlFormat;
		}
		
		override public function set width(value:Number):void
		{
			if(_autoResizeWidth)
				return;
			
			if(value == super.width)
				return;
			
			if(value > _maxBounds.width) {
				// todo implememnt dynamic
				throw new Error("max bounds are: " + _maxBounds.width);
			}
			super.width 							= value;
			_CompositionBounds.width 	= width;
			_isInvalid 								= true;
		}
		
		override public function set height(value:Number):void
		{
			if(_autoResizeHeight)
				return;
			
			if(value == super.height)
				return;
			
			if(value > _maxBounds.height) {
				// todo implememnt dynamic
				throw new Error("max bounds are: " + _maxBounds.height);
			}
			super.height 							= value;
			_CompositionBounds.height = value;
			_isInvalid 								= true;
		}
		
		public function get text():																		String						{	return _text;									}
		public function set text(value:String):												void
		{
			if(_text == value)
				return;
			trace(super.height);
			_text 			= value;
			_isInvalid 	= true;
		}
		
		public function get textLayoutFormat():												TextLayoutFormat	{	return _textLayoutFormat;			}
		public function set textLayoutFormat(value:TextLayoutFormat):	void
		{
			_textLayoutFormat = value;
			_isInvalid 				= true;
		}
		
		public function get autoResizeHeight():												Boolean						{	return _autoResizeHeight;			}
		public function set autoResizeHeight(value:Boolean):					void
		{
			_autoResizeHeight = value;
			_isInvalid				=	true;
		}
		
		public function get autoResizeWidth():												Boolean						{	return _autoResizeWidth;			}
		public function set autoResizeWidth(value:Boolean):						void
		{
			_autoResizeWidth = value;
			_isInvalid				=	true;
		}
		
		public function get actualContentBounds():										Rectangle					{	return _actualContentBounds		}
		
		public function get backgroundColor():												uint							{	return _backgroundColor;			}
		public function set backgroundColor(value:uint):							void							{	_backgroundColor = value;			}
		
	}
}
