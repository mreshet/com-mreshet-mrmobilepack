package com.mreshet.mrComps.textService.BidiTextField.bidi
{
	import com.mreshet.mrComps.textService.BidiTextField.bidi.core.db.BidiClassDB;

	public class Bidi
	{
		private var _mapUCtoBDC:BidiClassDB;
		private var _bidiFactory:BidiFactory;
		
		static private var _instance:Bidi = null; 
		
		public function Bidi()
		{
			if(_instance != null)
				throw new Error("Singleton!!!!");
				
			_instance = this;
			
			_mapUCtoBDC = new BidiClassDB();
			_mapUCtoBDC.loadDataBaseFromFile();
			
			_bidiFactory = new BidiFactory(_mapUCtoBDC);
		}
		
		public function bidiMe(input:String, bidiLevel:uint = 0):String
		{
			_bidiFactory.paragraphEmbeddingLevel = bidiLevel;
			_bidiFactory.textinput = input;
			_bidiFactory.startBidi();
			return _bidiFactory.textinput;
		}
		
		public function reorderLineSegment(sBoundIndex:uint = 0, eBoundIndex:int = -1):String
		{
			return _bidiFactory.reorderResolvedLevels(sBoundIndex, eBoundIndex);
		}
		
		public static function get instance():Bidi
		{
			return (_instance == null) ? new Bidi() : _instance;
		}

		
	}
}