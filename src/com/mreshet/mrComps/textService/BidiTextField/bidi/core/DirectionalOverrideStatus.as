package com.mreshet.mrComps.textService.BidiTextField.bidi.core
{
	public class DirectionalOverrideStatus
	{
		static public var Neutral:				String = "Neutral";
		static public var Right_To_Left:	String = "Right_To_Left";
		static public var Left_To_Right:	String = "Left_To_Right";
		
		public function DirectionalOverrideStatus()
		{
		}
	}
}