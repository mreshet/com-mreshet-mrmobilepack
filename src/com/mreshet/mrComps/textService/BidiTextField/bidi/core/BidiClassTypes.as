package com.mreshet.mrComps.textService.BidiTextField.bidi.core
{
	public class BidiClassTypes
	{
		static public var L:					String = "L";
		static public var LRE:				String = "LRE";
		static public var LRO:				String = "LRO";
		static public var R:					String = "R";
		static public var AL:					String = "AL";
		static public var RLE:				String = "RLE";
		static public var RLO:				String = "RLO";
		static public var PDF:				String = "PDF";
		static public var EN:					String = "EN";
		static public var ES:					String = "ES";
		static public var ET:					String = "ET";
		static public var AN:					String = "AN";
		static public var CS:					String = "CS";
		static public var NSM:				String = "NSM";
		static public var BN:					String = "BN";
		static public var B:					String = "B";
		static public var S:					String = "S";
		static public var WS:					String = "WS";
		static public var ON:					String = "ON";

		public function BidiClassTypes()
		{
		}
	}
}