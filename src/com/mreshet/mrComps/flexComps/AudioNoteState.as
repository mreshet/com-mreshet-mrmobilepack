package com.mreshet.mrComps.flexComps
{
	public final class AudioNoteState
	{
		public static const none:				String = 'none';
		public static const recording:	String = 'recording';
		public static const recorded:		String = 'recorded';
		public static const playing:		String = 'playing';
		public static const paused:			String = 'paused';
	}
}