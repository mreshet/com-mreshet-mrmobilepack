package com.mreshet.mrComps.flexComps.interfaces
{
	import starling.display.DisplayObject;

	/**
	 * the basic Interface for FlexComp responsive component
	 * @author Tomer Shalev
	 * 
	 */
	public interface IFlexComp extends IId
	{
		function get percentWidth():																Number;
		function set percentWidth(value:Number):										void;

		function get percentHeight():																Number;
		function set percentHeight(value:Number):										void;
		
		function get top():																					Number;
		function set top(value:Number):															void;
		
		function get bottom():																			Number;
		function set bottom(value:Number):													void;

		
		function get left():																				Number
		function set left(value:Number):														void;
		
		function get right():																				Number;
		function set right(value:Number):														void;
		
		/**
		 * override parent for calculations
		 */
		function get relativeCalcWidthParent():											DisplayObject;	
		function set relativeCalcWidthParent(value:DisplayObject):	void;
		
		function get relativeCalcHeightParent():										DisplayObject;	
		function set relativeCalcHeightParent(value:DisplayObject):	void;
		
		function get horizontalCenter():														Number;
		function set horizontalCenter(value:Number):								void;
		
		function get verticalCenter():															Number;
		function set verticalCenter(value:Number):									void;
		
		function get topPercentHeight():														Number;
		function set topPercentHeight(value:Number):								void;
		
		function get bottomPercentHeight():													Number;
		function set bottomPercentHeight(value:Number):							void;
		
		function get leftPercentWidth():														Number;
		function set leftPercentWidth(value:Number):								void;
		
		function get rightPercentWidth():														Number;
		function set rightPercentWidth(value:Number):								void;
		
		function get horizontalAlign():															String;
		function set horizontalAlign(value:String):									void;
		
		function get verticalAlign():																String;
		function set verticalAlign(value:String):										void;
		
		function get isSensitiveToParent():													Boolean;
		function set isSensitiveToParent(value:Boolean):						void;
		
		/**
		 * actions
		 */
		function applyAlignment():																	void;
		function layoutFlex():																			void;
	}
}