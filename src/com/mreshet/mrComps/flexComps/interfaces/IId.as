package com.mreshet.mrComps.flexComps.interfaces
{
	public interface IId
	{
		function get id():							String;
		function set id(value:String):	void;
	}
}