package com.mreshet.mrUtils.webMonitor
{
	import flash.events.Event;
	
	public class WebMonitorEvent extends Event
	{
		static public const CHANGE:	String = "CHANGE";
		
		private var _status:String = WebMonitorStatus.Status_Unknown;
		
		public function WebMonitorEvent($status:String)
		{
			super(CHANGE);
			
			_status = $status;
		}

		public function get status():String
		{
			return _status;
		}

	}
}