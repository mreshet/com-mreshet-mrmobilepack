package com.mreshet.mrUtils.webMonitor
{
	final public class WebMonitorStatus
	{
		static public const Status_Alive:			String = "Status_Alive";
		static public const Status_Dead:			String = "Status_Dead";
		static public const Status_Unknown:		String = "Status_Unknown";
		
		public function WebMonitorStatus()
		{
			throw new Error("do not instantiate this class!!!");
		}
	}
}