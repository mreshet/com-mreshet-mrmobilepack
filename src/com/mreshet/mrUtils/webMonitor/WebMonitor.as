package com.mreshet.mrUtils.webMonitor
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Timer;

	[Event(name="CHANGE", type="com.mreshet.mrUtils.webMonitor.WebMonitorEvent")]
	public class WebMonitor extends EventDispatcher
	{
		private var _ur:								URLRequest 	= null;
		private var _ul:								URLLoader 	= null;
		
		private var _timer:							Timer 			= null;
		private var _interval_seconds:	Number 			= 10; 
		
		private var _currentStatus:			String 			= WebMonitorStatus.Status_Unknown;
		private var _monitorUrl:				String 			= "http://www.cnn.com";
		
		public function WebMonitor()
		{
			_ur 		= new URLRequest(_monitorUrl);
			_ul 		= new URLLoader(_ur);
			_timer	=	new Timer(_interval_seconds*1000, 0);
		}
		
		public function get monitorUrl():String	{	return _monitorUrl;	}
		public function set monitorUrl(value:String):void
		{
			_monitorUrl = value;
			
			_ur.url			=	_monitorUrl;
		}

		public function start():void
		{
			addListeners();

			_timer.start();
		}
		
		public function stop():void
		{
			_timer.stop();
			
			addListeners(false);
		}
		
		public function dispose():void
		{
			addListeners(false);
			
			_ur 		= null;
			_ul 		= null;
			_timer 	= null;
		}
		
		public function get interval_seconds():Number	{	return _interval_seconds;	}
		public function set interval_seconds(value:Number):void
		{
			_interval_seconds = value;
			_timer.delay = _interval_seconds*1000;
		}
		
		protected function testConnection():void
		{
			_ul.load(_ur);
		}
		
		protected function onError(event:IOErrorEvent):void
		{
			trace("WebMonitor status: " + WebMonitorStatus.Status_Dead)
			
			if(_currentStatus != WebMonitorStatus.Status_Dead) {
				dispatchEvent(new WebMonitorEvent(WebMonitorStatus.Status_Dead));
			}
			
			_currentStatus = WebMonitorStatus.Status_Dead
		}
		
		protected function onComplete(event:Event):void
		{
			trace("WebMonitor status: " + WebMonitorStatus.Status_Alive)

			if(_currentStatus != WebMonitorStatus.Status_Alive) {
				dispatchEvent(new WebMonitorEvent(WebMonitorStatus.Status_Alive));
			}
			
			_currentStatus = WebMonitorStatus.Status_Alive
		}

		protected function addListeners(on:Boolean = true):void
		{
			if(on) {
				_ul.addEventListener(Event.COMPLETE, onComplete);
				_ul.addEventListener(IOErrorEvent.IO_ERROR, onError);
				
				_timer.addEventListener(TimerEvent.TIMER, onTimerUpdate);
			}
			else {
				_ul.addEventListener(Event.COMPLETE, onComplete);
				_ul.addEventListener(IOErrorEvent.IO_ERROR, onError);
				
				_timer.addEventListener(TimerEvent.TIMER, onTimerUpdate);
			}
		}
		
		protected function onTimerUpdate(event:TimerEvent):void
		{
			testConnection();
		}
		
	}
	
}