package com.mreshet.mrUtils
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.PNGEncoderOptions;
	import flash.display.Stage;
	import flash.display3D.Context3D;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.Stage;
	

	public class MrSnapShot
	{
		public static var stage:          flash.display.Stage = null;
		public static var context3D:      Context3D           = null;
		public static var gameInstance:   DisplayObject       = null;
		
		public function MrSnapShot()
		{
		}
		
		//stage:Stage, context3D:Context3D, gameInstance:DisplayObject
		
		public static function takeSnapshot():void {
			var bmpData:BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
			// Test if game use Starling and try to render its stage
			if (gameInstance.loaderInfo.applicationDomain.hasDefinition("starling.core::Starling") == true) {
				try {
					//This is where the magic happen
					var RenderSupport:Class = gameInstance.loaderInfo.applicationDomain.getDefinition("starling.core::RenderSupport") as Class;
					var support:Object = new RenderSupport();
					var Starling:Class = gameInstance.loaderInfo.applicationDomain.getDefinition("starling.core::Starling") as Class;
					//override Starling's rendering engine : force to clean and render
					Starling.context.clear();
					support.setOrthographicProjection(0, 0, stage.stageWidth, stage.stageHeight);
					Starling.current.root.render( support, 1.0 );
					support.finishQuadBatch();
					//force rendering now
					Starling.context.drawToBitmapData(bmpData);
					Starling.current.root.present();
					//draw stage to get both Stage3D and Native Stage drawn in the Bitmap
					//bmpData.draw(stage);
					}catch (err:Error) {
						trace("Error trying to render a Starling view", err);
					}
			}
			
			var date:Date				=	new Date();
			savePNG(bmpData, "scr_" + date.seconds.toString(), "gynonc");
			//savePNG(takeScreenshot(), "scr_" + date.seconds.toString(), "gynonc");
			//return bmpData;
		}
	
		public static function savePNG($bmpData:BitmapData, $fileName:String, $directory:String	=	"test"):void
		{
			var bmpd:BitmapData 				= $bmpData;//new BitmapData(_flashSwf.width,_flashSwf.height);
			var imgByteArray:ByteArray 	= new ByteArray;
			
			imgByteArray = bmpd.encode(new Rectangle(0,0,bmpd.width, bmpd.height),new PNGEncoderOptions());
			
			var directory:File	=	File.documentsDirectory.resolvePath($directory + "/temp.png");
			directory.parent.createDirectory();
			var fl:File 				= directory.parent.resolvePath($fileName + ".png");
					
			var fs:FileStream = new FileStream();
			try	{
				fs.open(fl,FileMode.WRITE);
				fs.writeBytes(imgByteArray);
				fs.close();
			}
			catch(e:Error)	{
				trace(e.message);
			}
		}

		public static function takeScreenshot(scl:Number=1.0):BitmapData
		{
			var stage: starling.display.Stage = Starling.current.stage;
			var width:Number = stage.stageWidth;
			var height:Number = stage.stageHeight;
			
			var rs:RenderSupport = new RenderSupport();
			
			rs.clear(stage.color, 1.0);
			rs.scaleMatrix(scl, scl);
			rs.setOrthographicProjection(0, 0, width, height);
			
			stage.render(rs, 1.0);
			rs.finishQuadBatch();
			
			var outBmp:BitmapData = new BitmapData(width*scl, height*scl, true);
			Starling.context.drawToBitmapData(outBmp);
			
			return outBmp;
		}
		
	}
}