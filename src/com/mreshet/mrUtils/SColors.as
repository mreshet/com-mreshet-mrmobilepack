package com.mreshet.mrUtils
{
	public class SColors
	{
		static public var BG_GREY:					uint = 0xE1E1E1;
		static public var TILE_GREY:				uint = 0xF4F4F4;
		static public var TILE_GREY_HARDER:	uint = 0xC9C9C9;
		static public var TEXT_GREY:				uint = 0x6F6F6F;
		static public var TEXT_GREY_LIGHT:	uint = 0xBBBEC3;
		static public var TEXT_GREY_MEDIUM:	uint = 0x989898;
		
		static public var PURPLE:						uint = 0xe515BA2;
		static public var DARK_BLUE:				uint = 0x142946;
		static public var DARKER_BLUE:			uint = 0x0C192B;
		static public var WHITE:						uint = 0xFFFFFF;
		static public var YELLOW:						uint = 0xE49D1C;
		static public var GREEN:						uint = 0x3DB656;
		static public var BLACK:						uint = 0x000000;
		static public var BLUE_LIGHT:				uint = 0x4683D3;
		static public var TRANSPARENT:			uint = 0x00000000;
		static public var REDDISH:					uint = 0xF7572E;
		static public var GREY_HARD:				uint = 0x484848;
		static public var GREY_SOFTER:			uint = 0x6F6E6F;
		static public var BLUEISH:					uint = 0x1B3962;
		
		
	}
}