package com.mreshet.ds.cache.core.types
{
	import flash.display.BitmapData;
	
	import com.mreshet.ds.cache.core.interfaces.ICachable;
	
	/**
	 * the base class 
	 * @author Tomer Shalev
	 * 
	 */
	public class CachedItem implements ICachable
	{
		private var _data: 					Object 		= null;
		private var _id:						String 		= null;
		private var _disposeMethod:	Function 	= null;
		
		public function CachedItem()
		{
		}
		
		public function dispose():void
		{
			if(_disposeMethod == dispose)
				throw new Error();

			if(_disposeMethod) {
				//var ff:Function= _data.dispose;
				_disposeMethod();
        trace("disposeddddddddddddddddddddddddddddddddddddddddddddddddddddddddd")
			}

      /*
      if(_data){
        _data.dispose();
        trace("disposeddddddddddddddddddddddddddddddddddddddddddddddddddddddddd")
      }*/
			
			_data						=	null;
			_id							=	null;
			_disposeMethod	=	null;
		}
		
		public function get data():													Object		{	return _data;						}
		public function set data(value:Object):							void			{	_data = value;					}
		
		public function get id():														String		{	return _id;							}
		public function set id(value:String):								void			{	_id = value;						}
		
		public function get disposeMethod():								Function	{	return _disposeMethod;	}
		public function set disposeMethod(value:Function):	void			{	_disposeMethod = value;	}

	}
}