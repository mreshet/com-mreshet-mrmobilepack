package com.mreshet.ds.cache.core.interfaces
{
  import com.mreshet.mrProcessManager.core.interfaces.IDisposable;
  import com.mreshet.mrProcessManager.core.interfaces.IId;
  
  public interface IIdDisposable extends IId, IDisposable
  {
    
  }
}